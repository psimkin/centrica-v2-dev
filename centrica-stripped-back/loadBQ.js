    // GCP + API params
     var PROJECT_ID = 'the-loom-centrica';
     var CLIENT_ID = '758435122706-qiqfefrbilh0q68m0d546cll0q17arm5.apps.googleusercontent.com';
     var API_KEY = 'AIzaSyD3a6qwVrP4EUpqtsv0VLWk8C0g1Kor6cc';
     var SCOPES = 'profile https://www.googleapis.com/auth/bigquery https://www.googleapis.com/auth/devstorage.full_control';
     var API_VERSION = 'v1';
     var tableParams = "reviews"
     var maxWords = 400;
     var BUCKET = "loom-staging";
     var PREFIX = "visualisations/scattertext";
     var company = "BG_UK";
     var principalIndex = 3;
     var baseURL = "https://storage.cloud.google.com/loom-staging/visualisations/scattertext/scattertext_BG_UK_";
     var bqResponse;
     var params; 

     // d3 
     let parseDate = d3.time.format("%b-%Y").parse;

     // general 
     let dropDownList;
     let isGeneric = false; 
     let isInit = false;

     init();

     function createDropDown(tableParams) {

        var request = gapi.client.storage.objects.list({
          'bucket' : BUCKET,
          'prefix' : PREFIX
               });
        // add source 
        //baseURL = baseURL + tableParams + "_";
        // run pipeline on response
        request.execute(function(response) {
          bqResponse = response;
          console.log(bqResponse);
          // get drop down date ranges
          getDropDownOptions(response, tableParams)
          // convert array to drop down selection
          //populateDropDown(dropDownList)

        })

      }


     /*
     * format BQ response
     */
     function getDropDownOptions(response, tableParams) {
          // clear array
          data = [];
          suffixes = [];
          mapping = [];
          // loop over each row & push to an array 
          response.items.forEach(function(d) {
          if (d.id.includes(company) && d.id.includes(tableParams)) {
                let obj = {};
                let fileName = d.id.split('/')[principalIndex];
                let dateRange = fileName.split("_")[4].split('.')[0];
                let parsed = parseDate(dateRange);
                // push parsed dates so we can order by date
                data.push(parsed);
                // push so we can reform original gs link
                suffixes.push(dateRange)
                // create a lookup so we can use a friendlier looking date 
                mapping.push({"og": dateRange, "parsed": parsed})
                }
              })

          // sort by date
          data = data.sort(function(a, b){
          return d3.descending(new Date(a), new Date(b));
                     });

          // format as a pretty string
          dropDownList = data.map(function(d) { return d.toString().split(' ')[3] + '-' + d.toString().split(' ')[1]});
          // get the first date & set as a global var
          params = dropDownList[0].split('-')[1] + '-' + dropDownList[0].split('-')[0];
           }

     /*
     * converts array to dropdown
     */
    function populateDropDown(dropDownList){
           $('.select-dropdown select').empty();
           for(var i=0; i< dropDownList.length;i++) {
              //creates option tag
               $("<option/>", {
                      class: "dateSelector",
                      value: dropDownList[i],
                      html: dropDownList[i],
                      }).appendTo('.select-dropdown select'); //appends to select if parent div has id dropdown
              }
           }

     /*
     * fixes the async gapi issues
     */
     function loadStorage() {
         gapi.client.load('storage', API_VERSION);
         setTimeout(function() {
              if (!gapi.client.storage) {
                // if doesn't load then try again 
                loadStorage();
              } else {
              // continue
              createDropDown(tableParams);
              }
            }, 500);
          }

     function init() {
    gapi.client.setApiKey(API_KEY);
    window.setTimeout(checkAuth, 1);
        }

    function checkAuth() {
        gapi.auth.authorize({
            client_id: CLIENT_ID,
            scope: SCOPES,
            immediate: true
        }, handleAuthResult).then(function () {
              gapi.client.setApiKey(API_KEY); 
              loadStorage();           
            });
    }

    function handleAuthResult(authResult) {
        if (authResult && !authResult.error) {
             //do something
        } else {
            $("#loginButton").click(function () {
                handleAuthClick();
            });

        }
    }

    function handleAuthClick() {
        gapi.auth.authorize({
            client_id: CLIENT_ID,
            scope: SCOPES,
            immediate: false
        }, handleAuthResult)
        return false;
    }
