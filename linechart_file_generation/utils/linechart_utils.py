
import pandas as pd
import numpy as np


def prepare_data(Data,remove_neutral = True):
    # get total size
    original_size = Data.shape[0]
    
    # eliminate neutral
    if remove_neutral:
        ind = Data["sentiment_scores"] != 0
        Data = Data[ind]
    
    # classify into positive and negative
    ind = Data["sentiment_scores"] > 0
    Data["cat"] = ""
    Data["cat"][ind],Data["cat"][~ind] = "Positive","Negative"    
    
    # eliminate the post deleted
    ind =  np.array([f.lower().find("post deleted")!=-1 for f in Data["text"].tolist()])
    Data = Data[~ind]
    
    return Data, original_size


def getMapping(df):
    
    df['keyphrases'] = df['keyphrases'].astype(str)
    
    # mapping between date and posts
    mappings = []
    keyphrase_mappings = []

    for date in df['date'].unique():
#        text = df[df['date'] == date]['text'].tolist()
        mappings.append({date: ' | '.join(df[df['date'] == date]['text'].tolist())})
        keyphrase_mappings.append({date: ' | '.join(df[df['date'] == date]['keyphrases'].tolist())})
        
    # flatten list of dictionaries
    text_map = {k: v for d in mappings for k, v in d.items()}
    keyphrase_map = {k: v for d in keyphrase_mappings for k, v in d.items()}
    
    return text_map, keyphrase_map


def createTS(df):
    
    # create TS output
    
    # filter etc.
    #df = data[data['company'] == company]
    #df = df[["cat","sentiment_scores","date", "text", "keyphrases", "base_table","source"]]
    df["date"] = pd.to_datetime(df["date"]).dt.strftime('%Y%m%d')

    # sort df
    df.sort_values(by="date",ascending = False,inplace = True)
    df.index = range(df.shape[0])
    #df['source'] = df['base_table'].map(data_source)
    
    outputs = []

    for source in df['source'].unique():
        # filter 
        tmp = df[df['source'] == source]
        pos = tmp[tmp["cat"] == 'Positive']
        neg = tmp[tmp["cat"] == 'Negative']

        # create mapping
        mapping_pos, kmapping_pos = getMapping(pos)
        mapping_neg, kmapping_neg = getMapping(neg)

        # apply mapping for positive reviews
        pos = pos.groupby(["cat","date"]).mean().reset_index().set_index("date")
        pos['date'] = pos.index
        pos['quotes'] = pos['date'].map(mapping_pos)
        pos['keywords'] = pos['date'].map(kmapping_pos)
        pos['source'] = source

        # apply mapping for negative reviews
        neg = neg.groupby(["cat","date"]).mean().reset_index().set_index("date")
        neg['date'] = neg.index
        neg['quotes'] = neg['date'].map(mapping_neg)
        neg['keywords'] = neg['date'].map(kmapping_neg)
        neg['source'] = source

        outputs.extend([neg, pos])
    
    return pd.concat(outputs), mapping_pos, mapping_neg
    


def reformat_for_linechart(Data,average_by = "week"):
    # reformat the data and export it to csv
    l = list(Data.groupby(["company","base_table"]))
    D = []
    parent = []
    for ll in l:
        try:
            name = "_".join(ll[0])
            d = ll[1][["cat","sentiment_scores","date"]]
            #d["date"] = d["date"].apply(lambda x: x[:10].replace("-",""))
            if average_by == "week":
                #d["date"] = pd.to_datetime(d["date"]).dt.strftime('%Yw%V')       
                d["date"] = pd.to_datetime(d["date"]).apply(lambda x: x - datetime.timedelta(days=x.isoweekday() % 7)).dt.strftime('%Y%m%d') 
            if average_by == "month":
                d["date"] = pd.to_datetime(d["date"]).apply(lambda x: x.replace(day=1)).dt.strftime('%Y%m%d') 
            elif average_by == "day":
                d["date"] = pd.to_datetime(d["date"]).dt.strftime('%Y%m%d')
                
            # sort
            d.sort_values(by="date",ascending = False,inplace = True)
            d.index = range(d.shape[0])
            
            # average accross every date
            d = d.groupby(["cat","date"]).mean().reset_index().set_index("date")
            d1 = pd.concat([l0[1]["sentiment_scores"] for l0 in list(d.groupby(["cat"]))],axis = 1, join = "outer")      
            
            # rename
            lbl = "Comp_" if name.lower().find("comp") != -1 else "Own_"
            d1.columns = [lbl +"Negative",lbl + "Positive"]
            #d1.columns = [name +"_Neg",name +"_Pos"]
            
            D.append(d1)
            #parent.append(name.split("_")[0].lower())
            parent.append(name)
            
        except Exception as e:
            print(e)
            
    return D, parent
  