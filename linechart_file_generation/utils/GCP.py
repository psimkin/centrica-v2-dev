import pandas as pd
import os
from google.cloud import storage
from google.cloud import bigquery


class GCP():
    def __init__(self,bucket_name = r"raw-data-staging", creds_location='../creds/creds.json'):
        self.bucket_name = bucket_name
        dir_path = os.path.dirname(os.path.realpath(__file__))
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join(dir_path,creds_location)
        self.call_gcp_client()

        
    def call_gcp_client(self):
        self.storage_client = storage.Client()
        
        # load the data from GC storage
        self.bucket = self.storage_client.bucket(self.bucket_name)
        
        # call the client
        self.big_query_client = bigquery.Client()

    def collect_blobs(self):
        blobs = self.bucket.list_blobs()
        # Collate data and remove unnecessary dependencies
        temp = []
        for file in blobs:
            file_path = r"gs://" + self.bucket_name + '/' + file.name
            if file.name.startswith("netbase"):
                if file.name.endswith("csv"):
                    temp.append([pd.read_csv(file_path, encoding='utf-8'),
                                 file.size,
                                 file.name,
                                 file.time_created])
                elif file.name.startswith("netbase") and file.name.endswith("xlsx"):
                    temp.append([pd.read_excel(file_path, encoding='utf-8'),
                                 file.size,
                                 file.name,
                                 file.time_created])
        return temp

    def grab_last_review(self, select_statement):
        try:
            query_job = self.big_query_client.query(select_statement)
            rows = query_job.result()
            for row in rows:
                result = row[0]
            return result
        except Exception as e:
            if type(e).__name__ == 'BadRequest':
                print('The input statement is incorrect: {statement}.'.format(statement=select_statement))
            else:
                print(e)

