#import numpy as np

#def other_fcn(request):
#    print(request)
#    return str(np.random.rand(10))

#def hello_world(request):
#    a = other_fcn(request)
#    return a

__author__ = "WT.Data.Science" 

from utils.GCP import GCP
import pandas as pd
#import numpy as np
#import os
#import copy
#import datetime
from utils.linechart_utils import createTS, prepare_data

def linechart_file_generation(request):
    # function and parameter definition
    # set the GCP environment
    gcp = GCP(bucket_name = r"loom-staging", creds_location='../creds/centrica-the-grid-creds.json')
    gcp.call_gcp_client()

    table = "analysed.merged_data_joint"
    days_to_analyse = 365
    max_texts = 100000
    mypath = r"gs://loom-staging/visualisations/assets"
    #mypath = r"C:\Users\DelgadoJ3\Desktop\assets"
  
    d = pd.read_gbq("SELECT DISTINCT company FROM " + table).values
    d = [dd[0] for dd in d if dd[0].find("competitors") == -1]

    for company in d:    
        print("Analysing: ",company)

        query = "SELECT sentiment_scores,text,date,base_table,keyphrases \
            FROM {} WHERE \
            company={} \
            AND CAST(date AS DATE) BETWEEN \
            DATE_SUB(current_date(),INTERVAL {} DAY) \
            AND current_date() ORDER BY date DESC LIMIT {}".format(table,
                                                                    company.join(["'","'"]),
                                                                    days_to_analyse,
                                                                    max_texts)

        # Load the data from BQ
        Data = pd.read_gbq(query)

        # prepare the data
        data,original_size = prepare_data(Data,remove_neutral = True)

    # trim the data for 2019 only
    #data["date"] = pd.to_datetime(Data["date"])
    #data = data[data["date"].between('2019-01-01', '2019-12-31')]
    #data.index = range(data.shape[0])

        data_source = {"netbase": "social", "trustpilot": "review", "consumeraffairs": "review"}
        data['source'] = data['base_table'].map(data_source)

    # dictionary data sources
#    companies = list(pd.read_gbq("SELECT DISTINCT company FROM analysed.merged_data_joint").values.reshape(-1))
 #   companies = [c for c in companies if c.find("competitors") ==-1]

        # format everything
        res,mapping_pos, mapping_neg = createTS(data)

        # save the csvs
        #outs = []
        for source in res['source'].unique():
            print(source)
            # generic timeseries
            tmp = res[(res['source'] == source)]
            tmp.rename_axis(None, inplace=True)
            generic_ts = pd.DataFrame(tmp.groupby('date')['sentiment_scores'].mean())
            generic_ts['date'] = generic_ts.index
            generic_ts['pos_quotes'] = generic_ts['date'].map(mapping_pos).astype(str)
            generic_ts['neg_quotes'] = generic_ts['date'].map(mapping_neg).astype(str)
            
#            generic_ts["pos_quotes"] = generic_ts["pos_quotes"].str.replace("\u200d","")
 #           generic_ts["neg_quotes"] = generic_ts["neg_quotes"].str.replace("\u200d","")
            #generic_ts['pos_quotes'] = generic_ts['pos_quotes'].apply(lambda x: x.decode("ascii"))#encode(encoding='ascii',errors='strict'))
            #generic_ts['neg_quotes'] = generic_ts['neg_quotes'].apply(lambda x: x.decode("ascii"))#encode(encoding='ascii',errors='strict'))

            generic_ts.to_csv(mypath + r"/" + company.split("_")[0] + '/linechart_generic_' + source + '.csv',
                              encoding='utf-8',
                              index = False)
            
        # old code
            for cat in res['cat'].unique():
                out = res[(res['source'] == source) & (res['cat'] == cat)]
                #out["quotes"] = out["quotes"].apply(lambda x: x.encode(encoding='utf-8',errors='strict'))
                #out["keywords"] = out["keywords"].apply(lambda x: x.encode(encoding='utf-8',errors='strict'))
                out.to_csv(mypath + r"/" + company.split("_")[0] + '/linechart_' + cat + '_' + source + '.csv', index=False)

#if __name__=="__main__":
#    linechart_file_generation("")
