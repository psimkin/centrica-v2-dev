from flask import Flask, jsonify, request, render_template, jsonify
from flask_cors import CORS, cross_origin
from oauth2client.service_account import ServiceAccountCredentials

# initialization
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app)

# gapi scopes
SCOPE = 'https://www.googleapis.com/auth/bigquery '
# key location
KEY_FILEPATH = 'gapi-client.json'

# generate access token
def get_access_token():
    return ServiceAccountCredentials.from_json_keyfile_name(
                                                            KEY_FILEPATH, SCOPE).get_access_token().access_token

@app.route('/', methods=['GET'])
def generate_access_token():
    tok = get_access_token()
    return jsonify({'access_token' : str(tok)})

if __name__ == '__main__':
    app.run()
