from google.cloud import language
import os
from google.cloud.language import enums
from google.cloud.language import types
from google.cloud import translate
import typing


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "creds/creds.json"
client = language.LanguageServiceClient()
translate_client = translate.Client()

# # Detect and send native Python encoding to receive correct word offsets.
# encoding = enums.EncodingType.UTF32
# if sys.maxunicode == 65535:
#     encoding = enums.EncodingType.UTF16
# result = client.analyze_entity_sentiment(document, encoding)


def analyze(texts: typing.List[str],
            languages: typing.List[str]):
    """Run a sentiment analysis request on text within a passed filename."""
    client = language.LanguageServiceClient()

    results = []
    translated_reviews = []

    for text, lang in zip(texts, languages):
        try:
            if lang in ["en", "es", "us"] and len(text.split(' ')) > 2:
                if lang == "us":
                    lang = "en"
                document = types.Document(
                    content=text,
                    type=enums.Document.Type.PLAIN_TEXT,
                    language=lang)
                output = client.analyze_entity_sentiment(document=document)
                translated_reviews.append('N/A')

                for entity in output.entities:
                    results.append({"entity": entity.name, "type": entity.type, "salience": entity.salience,
                                    "score": entity.sentiment.score})


            elif lang not in ["en", "es", "us"] and len(text.split(' ')) > 2:
                # clean text and translate if needed
                translation = translate_client.translate(text, target_language="en")
                translated_review = translation['translatedText']
                document = types.Document(
                    content=translated_review,
                    type=enums.Document.Type.PLAIN_TEXT,
                    language="en")
                output = client.analyze_entity_sentiment(document=document)
                translated_reviews.append(translated_review)

                for entity in output.entities:
                    results.append({"entity": entity.name, "type": entity.type, "salience": entity.salience,
                                    "score": entity.sentiment.score})

        except Exception as e:
            print(e)

    return results, translated_reviews

# def ReturnAveraged(self):
#     """ Get average entity score """
#
#     df = pd.concat(self.df)
#     df['freq'] = df['name'].apply(lambda x: df['name'].tolist().count(x))
#     return df.groupby('name').mean()
