# -*- coding: utf-8 -*-
"""
NLPClean: class to clean nlp data

Created on Mon Oct 28 14:35:47 2019

@author: Data Science team - Wunderman - Thompson
- Original script: Magda Zubala - Oct 2019
- Reformatted: Juan Delgado - Oct 2019

"""

# Load Libraries
import json
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

class NLPClean():
    def __init__(self,corpus,
                 params = {"lemmatise":False,"verbosity":False},
                 contractions_path = 'utils\contractions.json'):
        self.corpus = corpus
        self.check_input_format()
        self.params = params
        self.contractions_path = contractions_path
        
    def check_input_format(self):
        if isinstance(self.corpus,(list,tuple,np.ndarray,pd.Series)):
            ind = np.invert([isinstance(t,str) for t in self.corpus])
            self.corpus = np.array(self.corpus)
            self.corpus[ind] = ""
            if np.sum(ind):
                Warning(str(np.sum(ind)) + r"/"+ str(len(ind)) +" instances of input 'corpus' were not strings.")
        else:
            self.e = "The entry variable 'corpus' is not a list, tuple, numpy array or pandas series of strings."
            raise Exception(self.e)
    
    #Contraction expansion
    def expand_contractions(self,sentence, contractions_dict):
        text_list = sentence.split(' ')
        text_expanded = []
        for text in text_list:
            text = text.rstrip()
            if text in contractions_dict.keys():
                expanded = contractions_dict[text]
                text_expanded.append(expanded)
            else:
                text_expanded.append(text)
        return ' '.join(text_expanded)

    #Text cleaning
    def clean_text(self):
        # load word lemmatizer
        wordnet_lemmatizer = WordNetLemmatizer()
        # load contraction dictionary
        with open(self.contractions_path, encoding='utf-8', errors='ignore') as json_file:
            contraction_dict = json.load(json_file)
        # declare stop words
        stop_words = set(stopwords.words('english'))
        stop_words.update(['our', 'however', 'ocr', 'output', 'html', 'vol', 'also', 'and', 'the', 'but', 'in', 'arnt',
                           'many', 'cant', 'yes', 'no', 'etc', 'also'])
        print(stop_words) if self.params["verbosity"] else 0
        data = []
        max_length = 0
        for text in self.corpus:
            # 0. replace contractions
            text_expanded = self.expand_contractions(text, contraction_dict)
            # 1. tokenize
            word_tokens = word_tokenize(text_expanded)
            # 2. lemmatize
            word_lemms = [wordnet_lemmatizer.lemmatize(word) for word in word_tokens] if self.params["lemmatise"] else word_tokens
            cleantext = []
            for word in word_lemms:
                word = word.rstrip()
                # 4. remove stop words, two-letter words, punctuation and non alphabetic characters
                if word.lower() not in stop_words and word.isalpha():
                    # any additional cleaning
                    cleantext.append(word.lower())
            data.append(' '.join(cleantext))
            
            # return the length
            if len(cleantext) > max_length:
                max_length = len(cleantext)
            
        return data, max_length
        