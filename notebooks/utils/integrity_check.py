import pandas as pd
import numpy as np

class Integrity_netbase():
    # This is the integrity check for NETBASE data
    def __init__(self,gcp):
        self.blob_name = "netbase/integrity.json"
        self.Data = pd.DataFrame([])   
        self.gcp = gcp
        
    def load_integrity(self):
        # load the integrity file
        self.blob = self.gcp.bucket.blob(self.blob_name)
        self.integrity = eval(self.blob.download_as_string())    
        
    def get_new_temp(self,temp):    
        return [t for t in temp if not t[2] in self.integrity["file_names"]]
    
    def check_integrity(self,Data,temp,raw_col_names):    
        # read the log file - simple integrity
        test = [np.all([f in [t[2] for t in temp] for f in self.integrity["file_names"]]),
        np.all([f in [t[1] for t in temp] for f in self.integrity["file_sizes"]]),
        np.all([f in raw_col_names for f in self.integrity["column_names"]]),
        Data.shape[0] >= self.integrity["row_number"]]
        test += [np.all(test)]
        
        if test[-1]:
            print("All data integrity tests passed!")
        else:
            Warning("Data integrity tests not passed, These tests failed",np.array(list(self.integrity.keys()))[np.invert(test[:-1])])
        
        # create new data integrity framework
        self.integrity = {"file_names":[t[2] for t in temp],
        "file_sizes":[t[1] for t in temp],
        "column_names":['Sound Bite Text','Title','URL','Source Type','Author Location - Country 1',"Published Date (GMT+00:00) London","date_created","company"],
        "row_number":Data.shape[0]}
        
        # push to the cloud
        self.blob.upload_from_string(str(self.integrity))
