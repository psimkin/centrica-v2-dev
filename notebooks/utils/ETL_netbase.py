# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 15:05:11 2019

@author: DelgadoJ3
"""
# import libraries
import pandas as pd
import os
from google.cloud import storage
import numpy as np
from datetime import datetime

#%% Functions and classess
class GCP():
    def __init__(self,bucket_name = r"raw-data-staging",creds_location = 'creds\creds.json'):
        self.bucket_name = bucket_name
        dir_path = os.path.dirname(os.path.realpath(__file__))
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join(dir_path,creds_location)
        self.call_gcp_client()
        
    def call_gcp_client(self):
        self.storage_client = storage.Client()
        
        # load the data from GC storage
        self.bucket = self.storage_client.bucket(self.bucket_name)
        
    def collect_blobs(self):
        blobs = self.bucket.list_blobs()
        # Collate data and remove unnecessary dependencies
        temp = []
        for file in blobs:
            file_path = r"gs://" + self.bucket_name + '/' + file.name
            if file.name.startswith("netbase"):
                if file.name.endswith("csv"):
                    temp.append([pd.read_csv(file_path, encoding='utf-8'),
                                 file.size,
                                 file.name,
                                 file.time_created])
                elif file.name.startswith("netbase") and file.name.endswith("xlsx"):
                    temp.append([pd.read_excel(file_path, encoding='utf-8'),
                                 file.size,
                                 file.name,
                                 file.time_created])
        return temp

class Integrity():
    def __init__(self,gcp):
        self.blob_name = "netbase/integrity.json"
        self.Data = pd.DataFrame([])   
        self.gcp = gcp
        
    def load_integrity(self):
        # load the integrity file
        self.blob = self.gcp.bucket.blob(self.blob_name)
        self.integrity = eval(self.blob.download_as_string())    
        
    def get_new_temp(self,temp):    
        return [t for t in temp if not t[2] in self.integrity["file_names"]]
    
    def check_integrity(self,Data,temp,raw_col_names):    
        # read the log file - simple integrity
        test = [np.all([f in [t[2] for t in temp] for f in self.integrity["file_names"]]),
        np.all([f in [t[1] for t in temp] for f in self.integrity["file_sizes"]]),
        np.all([f in raw_col_names for f in self.integrity["column_names"]]),
        Data.shape[0] >= self.integrity["row_number"]]
        test += [np.all(test)]
        
        if test[-1]:
            print("All data integrity tests passed!")
        else:
            Warning("Data integrity tests not passed, These tests failed",np.array(list(self.integrity.keys()))[np.invert(test[:-1])])
        
        # create new data integrity framework
        self.integrity = {"file_names":[t[2] for t in temp],
        "file_sizes":[t[1] for t in temp],
        "column_names":['Sound Bite Text','Title','URL','Source Type','Author Location - Country 1',"Published Date (GMT+00:00) London","date_created"],
        "row_number":Data.shape[0]}
        
        # push to the cloud
        self.blob.upload_from_string(str(self.integrity))

def collate_transform_data(temp,reload_flag = False):
    # eliminate the files that have already been loaded
    temp = integrity.get_new_temp(temp) if reload_flag else temp
    
    # get the time of creation
    for ti,t in enumerate(temp):
        t[0]["date_created"] = t[3]
        temp[ti] = t
    
    # concatenate all the files
    Data = pd.concat([t[0] for t in temp],axis = 0)
    
    # get all the col names
    raw_col_names = np.unique([l for t in temp for l in list(t[0].keys())])
    
    # eliminate the cols that are not useful
    Data = Data[integrity.integrity["column_names"]]
    
    # rename columns
    Data.rename(columns = {'Sound Bite Text':"text",'Title':"title",'URL':"url",'Source Type':"source",'Author Location - Country 1':"country","Published Date (GMT+00:00) London":"date"}, inplace = True)
    
    # drop nans
    Data = Data.dropna(how = "all",subset = ['text', 'title', 'url', 'source', 'country', 'date'])
    Data.index = range(Data.shape[0])
    
    # reformat date
    Data["date"] = Data["date"].apply(lambda x: datetime.strptime(x, '%b %d, %Y %H:%M:%S %p'))
    
    return Data, raw_col_names


#%% function calls 
if __name__=="__main__":
    gcp = GCP()
    
    # ----------------------  Integrity constructor --------------------------------------
    integrity = Integrity(gcp)
    integrity.load_integrity()
        
    # ----------------------  Download Data --------------------------------------
    temp = gcp.collect_blobs()
      
    # ----------------------  Transform Data --------------------------------------
    Data, raw_col_names = collate_transform_data(temp)
    
    # ---------------------  Check Data integrity  --------------------------------------
    #integrity.check_integrity(Data,temp,raw_col_names)
    
    # ---------------------  Data cleaning  --------------------------------------
    
    
    # ---------------------  Load onto big query  -------------------------------------- 
    Data.to_gbq('base.netbase',if_exists = "replace")
