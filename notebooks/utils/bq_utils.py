import typing

from google.cloud import bigquery

def row_formatter(rows: typing.List[tuple],
                  new_cols: typing.List[list]):
    """Adds timestamp entry & new entry to tuple (needed for BQ upload)."""

    for col in new_cols:
        rows = [row + (new,) for row, new in zip(rows, col)]

    return rows


def insert_csv_to_table(client: typing.Type[bigquery.Client],
                        bq_table: typing.Type[bigquery.Table],
                        data_filename: str,
                        leading_rows: typing.Optional[int] = None) -> typing.Type[bigquery.job.LoadJob]:
    """Adds csv file contents to BigQuery table.
    """
    job_config = bigquery.LoadJobConfig()
    job_config.source_format = 'CSV'
    if leading_rows:
        job_config.skip_leading_rows = leading_rows
    with open(data_filename, 'rb') as data_file:
        job = client.load_table_from_file(data_file, bq_table, job_config=job_config)
    return job.result()


def insert_row_to_table(client: typing.Type[bigquery.Client],
                        bq_table: typing.Type[bigquery.Table],
                        row: typing.Union[typing.Tuple[typing.Any],
                                          typing.List[typing.Tuple[typing.Any]]]) -> typing.List[typing.Any]:
    """Inserts rows to BigQuery table """
    if isinstance(row, tuple):
        row = [row]
    schema = client.get_table(bq_table).schema
    errors = client.insert_rows(bq_table, row, schema)
    return errors


def select_from_table(client: typing.Type[bigquery.Client],
                      bq_table_name: str,
                      columns: typing.Optional[typing.List[str]] = None,
                      conditions: typing.Optional[typing.List[str]] = None) -> typing.List[typing.Tuple[typing.Any]]:
    """Performs a select statement on a BQ table."""
    select_statement = 'SELECT {cols} FROM {table}'.format(table=bq_table_name)
    if columns:
        select_statement.format(cols=','.join(columns))
    else:
        select_statement.format(cols='*')
    if conditions:
        select_statement += 'WHERE {cond}'.format(cond=' AND '.join(conditions))
    query_job = client.query(select_statement)
    result = list()
    for row in query_job:
        result.append(row)
    return result
