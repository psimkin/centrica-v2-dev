from google.cloud import language
import os
from google.cloud.language import enums
from google.cloud.language import types
import typing

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "creds/creds.json"
client = language.LanguageServiceClient()

def analyze(texts: typing.List[str]):

    """Run a sentiment analysis request on text within a passed filename."""
    client = language.LanguageServiceClient()

    sentiment_scores = []

    for text in texts:
        try:
            document = types.Document(
                content=text,
                type=enums.Document.Type.PLAIN_TEXT,
                language='en')

            output = client.analyze_sentiment(document=document)
            sentiment_scores.append({'score': output.document_sentiment.score,
                                     'magnitude': output.document_sentiment.magnitude})
        except Exception as e:
            print(e)
            sentiment_scores.append({'score': 0, 'magnitude': 0})

    scores = [x["score"] for x in sentiment_scores]
    magnitude = [x["magnitude"] for x in sentiment_scores]

    return scores, magnitude


