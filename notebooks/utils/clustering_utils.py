import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
import matplotlib.pyplot as plt
import seaborn as sns

def extract_topics(model, vec, sep=' | ', n=5):
    '''
        Extract topics in terms of vocabulary tokens from
        from a trained tokeniser and a trained NMF model.
        '''
    
    topics = {}
    
    # sort the array so that the most important tokens are first
    idx = model.components_.argsort(axis=1)[:, ::-1]
    
    # extract the most important tokens
    for i, t in enumerate(np.array(vec.get_feature_names())[idx]):
        topic = sep.join(t[:n])
        topics[i] = topic

    return topics

def tokenize(corpus, **kwargs):
    '''
        Simple wrapper function around a sklearn
        TfidfVectorizer object.
        '''
    # create an instance of the vectoriser
    tfidf = TfidfVectorizer(**kwargs)
    
    # the vectoriser returns a sparse array whicch
    # we convert to a dense array for convenience
    X_tfidf = np.asarray(tfidf.fit_transform(corpus).todense())
    
    print(f"Tokenised {X_tfidf.shape[0]} documents using a vocabulary of {len(tfidf.get_feature_names())} tokens.")
    
    return X_tfidf, tfidf

def plot_embedding(df_proj, xlim=None, ylim=None, figsize=(18, 18)):
    
    fig, ax = plt.subplots(figsize=figsize)
    sns.scatterplot(x='x',
                    y='y',
                    hue='topic',
                    data=df_proj,
                    palette='Paired',
                    alpha=0.8,
                    s=20,
                    ax=ax)
        
    leg = ax.legend(bbox_to_anchor = (1, 1), markerscale=2, frameon=False, prop={"size":17})
    leg.texts[0].set_text("")
    leg.set_title('Dominant topic', prop={"size":12})
    
    if xlim is not None:
        ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
        ax.get_legend().remove()
            
    ax.set_title('Topical portrait of LinkedIn', fontsize=27)
                            
    return ax

