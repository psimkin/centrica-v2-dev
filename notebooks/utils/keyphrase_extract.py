import pke
import typing


def keyphrase_extraction(texts: typing.List[str]):

    results = []

    for text in texts:
        try:
            # initialize & fit keyphrase extraction model (TopicRank)
            extractor = pke.unsupervised.TopicRank()
            extractor.load_document(input=text, language='en')
            extractor.candidate_selection(pos={'NOUN', 'PROPN', 'ADJ'})
            extractor.candidate_weighting(threshold=0.5,
                                          method='average')
            # return top five candidates
            results.append(' | '.join(
                [keyphrase for keyphrase, score in extractor.get_n_best(n=5, stemming=False)]))
        except Exception as e:
            print(e)
            results.append('')

    return results
