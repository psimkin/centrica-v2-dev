import lxml.html as html
import math
import time
import requests
import pandas as pd
from bs4 import BeautifulSoup
import datetime
import numpy as np


def scrape_trustpilot(review_site, company, last_review):
    
    # configs
    base_page = 'http://www.trustpilot.com/review/'
    review_page = base_page + review_site

    # trustpilot default 
    results_per_page = 20
    
    # get page, skipping HTTPS as it gives certificate errors
    page = requests.get(review_page, verify=False)
    tree = html.fromstring(page.content)

    # total amount of ratings
    rating_count = tree.xpath('//span[@class="headline__review-count"]')
    rating_count = int(rating_count[0].text.replace(',', ''))
    print('Found total of ' + str(rating_count) + ' reviews')

    # throttling to avoid spamming page with requests
    throttle = False
    sleep_time = 2

    # total pages to scrape
    pages = math.ceil(rating_count / results_per_page)
    print('Found total of ' + str(pages) + ' pages to scrape')
    
    data = pd.DataFrame(columns=['date', 'date_created', 'rating', 'text', 'company'])

    print('Processing..')
    for i in range(1, pages+1):

        if i > 998:
            break

        # Sleep if throttle enabled
        if throttle:
            time.sleep(sleep_time)

        # initialise request
        page = requests.get(review_page + '?page=' + str(i))
        tree = html.fromstring(page.content)

        # scrape text content 
        review_bodies = tree.xpath('//*[@class="review-content__text"]')
        bodies = [x.text_content().replace('\n', '').lstrip().rstrip() for x in review_bodies]

        # scrape scores
        soup = BeautifulSoup(page.content, "html.parser")
        all_scores = [x.find('img')['alt'].split(' ')[0] for x in soup.select('.review-content__header')]

        # scrape dates
        all_dates = [eval(x.find("script").text.replace("\n", "").replace("null", "0"))['publishedDate'] for x in soup.select('.review-content-header__dates')]
        all_dates = [datetime.datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ') for x in all_dates]

        # get date of download
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S UTC")

        finish = False
        for i, body in enumerate(bodies):
            if last_review == body:
                # find last review's index in bodies and take anything with lower index, ignoring anything with its index or higher
                bodies = bodies[0:i]
                all_scores = all_scores[0:i]
                all_dates = all_dates[0:i]
                finish = True
                break

        # append to dataframe
        data = data.append(pd.DataFrame(list(zip(bodies, all_scores, all_dates)), columns=['text', 'rating', 'date']))
        data['date_created'] = date
        print(data.shape)
        if finish:
            break

    print('Processed ' + str(data.shape) + 'ratings.. Finished!')
    data['company'] = company
    data = data[data['rating'].str.contains("#") == False]
    return data


def scrape_consumeraffairs(reviewPage, fn, last_review):
    
    data = pd.DataFrame(columns=['date', 'date_created', 'rating', 'text', 'company'])

    page = requests.get("https://www.consumeraffairs.com/" + reviewPage + '?page=' + str(1))
    tree = html.fromstring(page.content)
    # total amount of ratings
    rating_count = [x.text_content() for x in tree.xpath('//figcaption[@class="ca-txt-cpt prf-rtng__cptn"]/span')]
    #print(rating_count)
    results_per_page = 25

    # total pages to scrape
    pages = math.ceil(int(rating_count[0]) / results_per_page)
    print('Found total of ' + str(pages) + ' pages to scrape')

    print('Processing..')
    for i in range(1, pages):

        # Sleep if throttle enabled
        #if(throttle): time.sleep(sleepTime)

        page = requests.get("https://www.consumeraffairs.com/" + reviewPage + '?page=' + str(i))
        tree = html.fromstring(page.content)

        # retrieve review bodies & datetime
        final_dates = []
        review_bodies = tree.xpath('//div[@class="rvw-bd ca-txt-bd-2"]')
        dates = [x.text_content().split(': ')[1] for x in tree.xpath('//span[@class="ca-txt-cpt ca-txt--clr-gray"]')]
        for date in dates:
            date = date.replace('Sept', 'Sep').replace('July', 'Jul').replace('March', 'Mar').replace('April', 'Apr').replace('June', 'Jun')
            try:
                new_date = datetime.datetime.strptime(date, '%b. %d, %Y')
            except:
                new_date = datetime.datetime.strptime(date, '%b %d, %Y')
            finally:
                final_dates.append(new_date)

        reviews = []

        # remove datetime from post
        for n, review in enumerate(review_bodies):
            cleaned = review.text_content().replace(dates[n], '').replace("Original review:", '')
            reviews.append(cleaned)

        # get date of download
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S UTC")

        # retrieve ratings
        soup = BeautifulSoup(page.content, "html.parser")
        rating_strs = [x.get("data-rating") for x in soup.find_all(class_='stars-rtg stars-rtg--sm')]
        ratings = [x.split('.')[0] for x in rating_strs if '.' in x]

        finish = False
        for i, body in enumerate(reviews):
            if last_review == body:
                # find last review's index in bodies and take anything with lower index, ignoring anything with its index or higher
                reviews = reviews[0:i]
                ratings = ratings[0:i]
                final_dates = final_dates[0:i]
                finish = True
                break

        data = data.append(pd.DataFrame(list(zip(reviews, ratings, final_dates)), columns=['text', 'rating', 'date']))
        data['date_created'] = date
        print(data.shape)
        if finish:
            break

    print('Processed ' + str(data.shape) + 'ratings.. Finished!')
    data['company'] = fn
    return data


def collate_transform_netbase(temp,integrity,reload_flag = False):
    # integrity is an integrity constructor

    # eliminate the files that have already been loaded
    temp = integrity.get_new_temp(temp) if reload_flag else temp
    
    # get the time of creation
    for ti,t in enumerate(temp):
        t[0]["date_created"] = t[3]
        t[0]["company"] = t[2].split(".")[0].split(r"/")[1] # change to company name from file name
        temp[ti] = t
    
    # concatenate all the files
    Data = pd.concat([t[0] for t in temp],axis = 0)
    
    # get all the col names
    raw_col_names = np.unique([l for t in temp for l in list(t[0].keys())])
    
    # eliminate the cols that are not useful
    Data = Data[integrity.integrity["column_names"]]
    
    # rename columns
    Data.rename(columns = {'Sound Bite Text':"text",'Title':"title",'URL':"url",'Source Type':"source",'Author Location - Country 1':"country","Published Date (GMT+00:00) London":"date"}, inplace = True)
    
    # drop nans
    Data = Data.dropna(how = "all",subset = ['text', 'title', 'url', 'source', 'country', 'date'])
    Data.index = range(Data.shape[0])
    
    # reformat date
    Data["date"] = Data["date"].apply(lambda x: datetime.datetime.strptime(x, '%b %d, %Y %H:%M:%S %p'))
    
    return Data, raw_col_names