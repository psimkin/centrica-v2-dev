import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage

# Set the cloud client up
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'creds/creds.json'
storage_client = storage.Client()
bucket_name = r"raw-data-staging"

# load the data from GC storage
bucket = storage_client.bucket(bucket_name)
blobs = bucket.list_blobs()

def blob_reader(bucket, fn):

	data = pd.DataFrame()

	# Collate data and remove unnecessary dependencies
	temp = []
	for file in blobs:
    	filename = file.name
    	if filename.startswith(fn) and filename.endswith("csv"):
        	data = data.append(pd.read_csv(r"gs://" + bucket_name + '/' + filename, encoding='utf-8'))

   return data