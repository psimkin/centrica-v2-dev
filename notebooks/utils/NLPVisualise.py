# -*- coding: utf-8 -*-
"""
NLPVisualise: class to visualise nlp data

Created on Mon Oct 28 14:35:47 2019

@author: Data Science team - Wunderman - Thompson

- Original script: Magda Zubala - Oct 2019
- Reformatted: Juan Delgado - Oct 2019


"""
import scattertext as st
import spacy

class NLPVisualise():
    def __init__(self,corpus,
                 output_filename = "scatterplot",
                 scatter_labels = {"cat":"x","not_cat":"y","text_col":"text","category_col":"cat"}):       
        
        self.corpus = corpus
        #self.check_input_corpus()
        self.output_filename = output_filename
        self.scatter_labels = scatter_labels
        
    """   This functions is disabled for now 
    def check_input_corpus(self):
        
        # transform input into pandas
        self.corpus = pd.DataFrame(self.corpus,columns = ["cat","text"])
       """ 
    def scatter_text(self,thr:int = 0):
    
        nlp = spacy.load('en_core_web_sm')
    
        # get the corpus
        self.corpus = st.CorpusFromPandas(self.corpus,
                                          category_col=self.scatter_labels['category_col'],
                                          text_col=self.scatter_labels['text_col'],
                                          nlp=nlp).build()
        if thr > 0:
            #print(thr)
            self.corpus = self.corpus.remove_terms_used_in_less_than_num_docs(threshold=thr)
        
        # build ands save the html
        html = st.produce_scattertext_explorer(self.corpus,
                                                   category=self.scatter_labels["cat"],
                                                   category_name=self.scatter_labels["cat"],
                                                   not_category_name = self.scatter_labels["not_cat"],)
    
        
        open(self.output_filename + ".html", 'wb').write(html.encode('utf-8'))
        

